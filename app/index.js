const config = require('../config');
const io = require('socket.io')(config.socket.port, config.socket.options);
const db = require('mongoose');

db.connect(config.database.uri, { useMongoClient: true });

require('./models')(db);
require('./sockets')(io);
