const routeSchema = require('./schemas/route');
const userSchema = require('./schemas/user');

module.exports = (db) => {
  db.Promise = global.Promise;

  module.exports.Route = db.model('Route', routeSchema);
  module.exports.User = db.model('User', userSchema);
};
