const db = require('mongoose');
const Schema = db.Schema;

class User {
  static createUser(email) {
    return this.create({
      _id: new db.Types.ObjectId(),
      email,
      routes: []
    });
  }

  static async getByEmail(email) {
    return await this.findOne({ email }).populate('routes').exec() || this.createUser(email);
  }
}

const userSchema = new db.Schema({
  _id: Schema.Types.ObjectId,
  email: { type: String, lowercase: true },
  routes: [{ type: Schema.Types.ObjectId, ref: 'Route' }]
});

userSchema.loadClass(User);

module.exports = userSchema;
