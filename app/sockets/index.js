const routes = require('./routes');
const notifications = require('./notifications');

module.exports = (io) => {
  io.listenerDecorator = eventListenerDecorator;
  routes(io);
  notifications(io);
};

function eventListenerDecorator(socket) {
  return (evt, callback) => {
    socket.on(evt, async(...payload) => {
      try {
        const result = await callback(payload);

        result && socket.emit(`${evt}-resp`, result);
      } catch (exception) {
        socket.emit('bad-request', { message: `Request with name ${evt} failed, cause: ${exception}` });
        throw exception;
      }
    });
  };
}
