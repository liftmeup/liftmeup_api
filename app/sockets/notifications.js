let notification;

module.exports = (io) => {
  const nio = io.of('/notifications');

  notification = new Promise((resolve) => {
    nio.on('connection', (socket) => {
      socket.join('global');
      notification = { socket, nio };
      resolve(notification);
    });
  });
};

async function notify(body) {
  const { socket } = await notification;
  socket.emit('notify', body);
}

async function notifyAll(body) {
  const { nio } = await notification;
  nio.to('global').emit('notify', body);
}

module.exports.notify = notify;
module.exports.notifyAll = notifyAll;
